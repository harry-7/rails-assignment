class IssuesController < ApplicationController
  before_action :set_issue, only: [:show, :edit, :update, :destroy]

  # GET /issues
  # GET /issues.json
  def index
    project=Project.find(params[:project_id])

    @issues = project.issues
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @issues }
    end
  end

  # GET /issues/1
  # GET /issues/1.json
  def show
    project=Project.find(params[:project_id])

    @issue = project.issues.find(params[:id])
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @issue }
    end
  end

  # GET /issues/new
  def new
    project=Project.find(params[:project_id])
    @project_id=params[:project_id]
    @issue=project.issues.build
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @issue }
  end
  end
  # GET /issues/1/edit
  def edit

    project=Project.find(params[:project_id])
    @issue = project.issues.find(params[:id])
    if @issue.user_id!=current_user.id
      flash[:notice]="No privilleges to edit"
      flash[:color]='invalid'
      redirect_to "/projects/#{project.id}/issues"

  end
  end

  # POST /issues
  # POST /issues.json
  def create
    if user_signed_in?
       project=Project.find(params[:project_id])
       @issue = project.issues.create(issue_params)
      email=current_user.email
      @issue.add_details(email,project.name,project.id,current_user.id)
     respond_to do |format|
      if @issue.save
        format.html { redirect_to [@issue.project,@issue, notice: 'Issue was successfully created.'] }
        format.json { render :show, status: :created, location: @issue }
      else
        format.html { render :action => "new"}
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
    else
      flash[:notice]="Signup or log in to create as issue"
      flash[:color]="invalid"
      redirect_to "/projects"
    end

  end

  # PATCH/PUT /issues/1
  # PATCH/PUT /issues/1.json
  def update
    project = Project.find(params[:project_id])
    #2nd you retrieve the comment thanks to params[:id]
    @issue = project.issues.find(params[:id])
    respond_to do |format|
      if @issue.update(issue_params)
        format.html { redirect_to([@issue.project, @issue], :notice => 'issue was successfully updated.') }
        format.json { render :show, status: :ok, location: @issue }
      else
        format.html { render :action => "edit" }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /issues/1
  # DELETE /issues/1.json
  def destroy
    project = Project.find(params[:project_id])
    @issue = project.issues.find(params[:id])
    if @issue.user_id==current_user.id or project.user_id==current_user.id
      puts"\n\n\n #{@issue.user_id}  #{project.user_id}   #{current_user.id}\n\n\n"
      @issue.destroy
   respond_to do |format|
      format.html { redirect_to(project_issues_url) }
      format.xml  { head :ok }
   end
  else
    flash[:notice]="No privilleges to delete"
    flash[:color]='invalid'
    redirect_to "/projects/#{project.id}/issues"
  end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_issue
      @issue = Issue.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def issue_params
      params.require(:issue).permit(:subject, :description, :issued_by, :project_name, :project_id, :user_id)
    end
end
