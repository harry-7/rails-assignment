class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  # GET /comments
  # GET /comments.json
  def index
    #@comments = Comment.all
    issue=Issue.find(params[:issue_id])
    @comments=issue.comments
    respond_to do |format|
      format.html
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    issue=Issue.find(params[:issue_id])
    comment=issue.comments.find(params[:id])
    respond_to do |format|
      format.html
    end
  end

  # GET /comments/new
  def new
    #@comment = Comment.new
    issue=Issue.find(params[:issue_id])

    @comment=issue.comments.build
    respond_to do |format|
      format.html
    end
  end

  # GET /comments/1/edit
  def edit
    issue=Issue.find(params[:issue_id])
    @comment=issue.comments.find(params[:id])
    if current_user.id != issue.user_id
      flash[:notice]="Only issue owner can Remove/edit comments"
      flash[:color]="Invalid"
      redirect_to([@comment.issue.project ,@comment.issue])
    end
  end

  # POST /comments
  # POST /comments.json
  def create
   # @comment = Comment.new(comment_params)
    issue=Issue.find(params[:issue_id])
    @comment=issue.comments.create(comment_params)
    respond_to do |format|
      if @comment.save
        format.html { redirect_to [@comment.issue.project ,@comment.issue,@comment], :notice => 'Comment was successfully created.' }
        format.json { render :show, status: :created, location: @comment }
      else
        format.html { render :action =>"new" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    issue=Issue.find(params[:issue_id])
    @comment=issue.comments.find(params[:id])
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to ([issue.project,issue]),  :notice => 'Comment was successfully updated.' }
        format.json { render :show, :status => :ok, location: @comment }
      else
        format.html { render :action =>"edit" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    issue=Issue.find(params[:issue_id])
    if current_user.id == issue.user_id
      @comment=issue.comments.find(params[:id])
        @comment.destroy
    respond_to do |format|
      format.html { redirect_to([issue.project,issue])}
      format.json { head :ok }
    end
    else
      flash[:notice]="Only issue owner can Remove/edit comments"
      flash[:color]="Invalid"
      redirect_to ([issue.project,issue])
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:commenter, :body, :issue_id)
    end
end
