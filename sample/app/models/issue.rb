class Issue < ActiveRecord::Base
  belongs_to :project
  belongs_to :user
  has_many :comments
  def add_details(email,name,id,userid)
    self.issued_by=email
    self.project_name=name
    self.project_id=id
    self.user_id=userid
  end
end
