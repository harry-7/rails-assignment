class Comment < ActiveRecord::Base
  belongs_to :issue
  validates :commenter, :presence => true
  validates :body, :presence => true
end
