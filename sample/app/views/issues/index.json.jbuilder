json.array!(@issues) do |issue|
  json.extract! issue, :id, :subject, :description, :issued_by, :project_name, :project_id, :user_id
  json.url issue_url(issue, format: :json)
end
