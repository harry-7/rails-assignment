class IssuesController < ApplicationController
def create
  if user_signed_in?
    @project=Project.find(params[:project_id])
    @issue=@project.issues.create(issue_params)
    email=current_user.email
    @issue.add_details(email,@project.name)

    #redirect_to project_path(@project)
  else
    flash[:notice]="Sign in to add an issue"
    flash[:color]="invalid"
    redirect_to project_path(@project)
    end
  end
  private
    def issue_params
      params.require(:issue).permit(:subject, :description, :user_id,:project_id,:issuer_name,:project_name)
    end
end
